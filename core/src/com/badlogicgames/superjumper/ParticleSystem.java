package com.badlogicgames.superjumper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * Created by victor.canas on 05/04/2017.
 */

public class ParticleSystem
{
    public ParticleEffect pe;
    public SpriteBatch batch;
    private String fileParticle;
    private String fileImg;
    public float x;
    public float y;
    public ParticleSystem(String fileNameParticle, String fileNameImg, float x, float y)
    {
        fileParticle=fileNameParticle;
        fileImg=fileNameImg;
        this.x=x;
        this.y=y;
        batch = new SpriteBatch();
        pe = new ParticleEffect();
        pe.load(Gdx.files.internal(fileParticle),Gdx.files.internal(fileImg));
        pe.setPosition(x, y);

        pe.start();
    }
    public void update(float bob_x, float bob_y)
    {
        pe.setPosition(bob_x+x, bob_y+y);
    }
    public void render()
    {
        pe.update(Gdx.graphics.getDeltaTime());
        batch.begin();
        pe.draw(batch);
        batch.end();
    }
}
